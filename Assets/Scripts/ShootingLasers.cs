﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShootingLasers : MonoBehaviour{

    public Vector3 laserOffSet = new Vector3(0, 0.5f, 0);

    public GameObject laserPrefab;

    public float fireDelay = 0.25f;
    float cooldownTimer = 0;


	// Use this for initialization
	void Start ()
    {
		
	}
	
	// Update is called once per frame
	void Update ()
    {
        cooldownTimer -= Time.deltaTime;

        if (Input.GetKeyDown(KeyCode.Space) && cooldownTimer <= 0)
        {
            cooldownTimer = fireDelay;

            Vector3 offset = transform.rotation * laserOffSet;

            //From where the laser is shot. Erase the + and offset if you want to shoot from the center of the ship.
            Instantiate(laserPrefab, transform.position + offset, transform.rotation);
        }
	}
}