﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RedLaser : MonoBehaviour {

    //The Speed of the laser
    public float maxSpeed = 5f;

    //The Timer before destruction for the laser.
    public float timer = 1f;

	// Use this for initialization
	void Start ()
    {
		
	}
	
	// Update is called once per frame
	void Update ()
    {
        //How fast the laser travels.
        Vector3 pos = transform.position;

        Vector3 velocity = new Vector3(0, maxSpeed * Time.deltaTime, 0);

        pos += transform.rotation * velocity;
        transform.position = pos;

        timer -= Time.deltaTime;
        if (timer <= 0)
        {
            Destroy(gameObject);
        }
	}

    //If the laser goes off the level, it will be destroyed.
    void OnTriggerExit2D(Collider2D other)
    {
        if (other.gameObject.tag == "Board")
        {
            Destroy(this.gameObject);
        }
    }

    //If the laser manages to hit the enemy ship, it will be destroyed.
    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.tag == "EnemyShip")
        {
            Destroy(gameObject);
        }
    }
}
