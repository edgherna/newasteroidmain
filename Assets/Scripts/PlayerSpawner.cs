﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerSpawner : MonoBehaviour {

    public GameObject playerPrefab;
    GameObject playerInstance;

    //Number of total lives for the player.
    public int numLives = 4;

    //How long it takes for the player to be put back in the game.
    public float respawnTimer;

	// Use this for initialization
	void Start ()
    {
        SpawnPlayer();
	}

    //This module will respawn the player after the player has died.
    void SpawnPlayer()
    {
        numLives--;
        playerInstance = (GameObject)Instantiate(playerPrefab, transform.position, Quaternion.identity);
    }
	
	// Update is called once per frame
	void Update ()
    {
        //This is the timer for the player that has to wait before it is spawned again.
		if (playerInstance == null && numLives > 0)
        {
            respawnTimer -= Time.deltaTime;

            if (respawnTimer <= 0)
            {
                SpawnPlayer();
            }
        }
	}

    void OnGUI()
    {
        //A GUI will appear based on how the game is going.
        if (numLives > 0 || playerInstance != null)
        {
            GUI.Label(new Rect(0, 0, 100, 50), "Lives Left: " + numLives);
        }
        else
        {
            GUI.Label(new Rect(Screen.width / 2 - 50, Screen.height/2 - 25 , 100, 50), "Game Over");
        }
    }
}
