﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChasePlayer : MonoBehaviour {

    //The focus for the enemy ship.
    Transform player;

    //To control his rotation speed.
    public float rotationSpeed = 180f;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update ()
    {
		if (player == null)
        {
            //Find the player ship.
            GameObject go = GameObject.Find("RedShip(Clone)");

            if (go != null)
            {
                player = go.transform;
            }
        }

        //Determines if the player exists.
        if (player == null)
        {
            return;
        }

        //When the player is found, it turns to face it.
        Vector3 direction = player.position - transform.position;
        direction.Normalize();

        float zAngle = Mathf.Atan2(direction.y, direction.x) * Mathf.Rad2Deg - 90;

        Quaternion desiredRotation = Quaternion.Euler(0, 0, zAngle);
        transform.rotation = Quaternion.RotateTowards(transform.rotation, desiredRotation, rotationSpeed * Time.deltaTime);
	}
}
