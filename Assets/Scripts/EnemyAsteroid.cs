﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyAsteroid : MonoBehaviour
{
    //The direction from where the asteroid is going.
    public Vector2 direction;

	// Use this for initialization
	void Start ()
    {
		
	}
	
	// Update is called once per frame
	void Update ()
    {
        //The instantiation of the Asteroid.
        direction = new Vector3(Random.Range(0, 1f), Random.Range(0, 1f), 0);
        transform.Translate(direction * Time.deltaTime * GameManager.instance.enemyShipSpeed);
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.tag == "Bullet")
        {
            GameManager.instance.activeEnemies.Remove(this.gameObject);

            //Destroy the Asteroid.
            Destroy(this.gameObject);

            EnemySpawner.enemyCount--;

            //Destroy the Bullet after it collides with the Asteroid.
            Destroy(other.gameObject);
        }
    }

    //If the asteroid goes off the boundaries, it will be destroyed.
    void OnTriggerExit2D(Collider2D other)
    {

        if (other.gameObject.tag == "Board")
        {
            Destroy(this.gameObject);
            EnemySpawner.enemyCount--;
        }
    }
}