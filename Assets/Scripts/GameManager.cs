﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour {

    //Singleton
    public static GameManager instance;

    //Enemy Spawn List
    public GameObject[] enemySpawnPoints;

    //For the Enemy Ship prefabs
    public float enemyShipSpeed;
    public float enemyShipRotationSpeed;
    public List<GameObject> enemyShips;

    //For the Asteroid prefabs
    public float enemyAsteroidSpeed;
    public List<GameObject> enemyAsteroids;

    //List of enemies
    public List<GameObject> activeEnemies;
    public bool removingEnemies;
    public int maximumNumberActiveEnemies;
    
    void Awake()
    {
        if (instance == null)
        {
            instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(gameObject);
        }
    }

    // Use this for initialization
    void Start ()
    {
        activeEnemies = new List<GameObject>();
        removingEnemies = false;
	}
	
	// Update is called once per frame
	void Update ()
    {
        /*
        if (activeEnemies.Count < 3)
        {
            AddEnemy();
        }
        
        
        if ()
        {
            RemoveEnemy();
        }
        */

        if (Input.GetKey(KeyCode.Escape))
        {
            Application.Quit();
        }
	}

    void AddEnemy()
    {
        if (activeEnemies.Count < maximumNumberActiveEnemies)
        {
            //Decide the Spawn Point
            int id = Random.Range(0, enemySpawnPoints.Length);
            GameObject point = enemySpawnPoints[id];

            //Initialize enemyAsteroid
            GameObject enemyAsteroid = new GameObject();

            //Instantiate the enemy ships
            GameObject enemyAsteroidInstance = Instantiate<GameObject>(enemyAsteroid, point.transform.position, Quaternion.identity);
            Vector2 directionVector = new Vector2(Random.Range(-1.0f, 1.0f), Random.Range(-1.0f, 1.0f));
            directionVector.Normalize();

            if (enemyAsteroidInstance.GetComponent<EnemyAsteroid>() != null)
            {
                enemyAsteroidInstance.GetComponent<EnemyAsteroid>().direction = directionVector;
            }

            //Add enemiesShips to list
            activeEnemies.Add(enemyAsteroidInstance);
        }
    }
}
