﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawner : MonoBehaviour
{
    //This is the prefab for the enemy Ships.
    public GameObject enemyPrefab;

    float spawnDistance = 5f;
    public float enemyRate = 5;
    public float nextEnemy = 1;

    //This integer is to keep count of the enemies that are within the level.
    static public int enemyCount = 0;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update ()
    {
        //This is to control the spawning of the enemy ships.
        nextEnemy -= Time.deltaTime;

        if (nextEnemy <= 0 && enemyCount < 3)
        {
            nextEnemy = enemyRate;
            enemyRate *= 0.9f;
            if (enemyRate < 2)
            {
                enemyRate = 2;
            }

            //This will help the enemy ship spawn from within the level.
            Vector3 offset = Random.insideUnitSphere;
            offset.z = 0;
            offset = offset.normalized * spawnDistance;
            Instantiate(enemyPrefab, transform.position + offset, Quaternion.identity);
            enemyCount++;
        }
    }

}