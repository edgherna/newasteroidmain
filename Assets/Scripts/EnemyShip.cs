﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyShip : MonoBehaviour
{
    //From where the Enemy Ship is looking at.
    public Vector3 direction;

    //The health of the enemies. If health is zero, it means instant death at startup.
    public int health;
	
    // Use this for initialization
	void Start ()
    {
        
	}
	
	// Update is called once per frame
	void Update ()
    {
        //This will make the ship move. If the player isn't around, it will go in a straight line.
        direction = new Vector3(0, 1, 0);
        transform.Translate(direction * Time.deltaTime * GameManager.instance.enemyShipSpeed);

        //If their health reaches less than or equal to zero, it dies.
        if (health <= -1)
        {
            Death();
        }
    }

    void OnTriggerEnter2D()
    {
        //It loses one health point.
        health--;
    }

    void Death()
    {
        //When its health points reach -1, it dies.
        Destroy(gameObject);
        EnemySpawner.enemyCount--;
    }

    //For this module, if the enemy ship goes off the boundaries, it will be destroyed.
    void OnTriggerExit2D(Collider2D other)
    {
        if (other.gameObject.tag == "Board")
        {
            Destroy(this.gameObject);
            EnemySpawner.enemyCount--;
        }
    }
}
