﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyAsteroidSpawner : MonoBehaviour
{
    //The prefab for the Asteroids.
    public GameObject enemyPrefab;

    //Controls where the Asteroids will spawn on the level.
    float spawnDistance = 5f;
    public float enemyRate = 5;
    public float nextEnemy = 1;

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        //Timer for when the Asteroids will spawn.
        nextEnemy -= Time.deltaTime;

        if (nextEnemy <= 0 && EnemySpawner.enemyCount < 3)
        {
            nextEnemy = enemyRate;
            enemyRate *= 0.9f;
            if (enemyRate < 2)
            {
                enemyRate = 2;
            }

            //The Asteroid will spawn in a random location from within the level.
            Vector3 offset = Random.insideUnitSphere;
            offset.z = 0;
            offset = offset.normalized * spawnDistance;
            Instantiate(enemyPrefab, transform.position + offset, Quaternion.identity);
            EnemySpawner.enemyCount++;
        }
    }

}
