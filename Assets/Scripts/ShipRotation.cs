﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShipRotation : MonoBehaviour
{
    //This is the stats for the player's ship, the speed and rotation.
    public float maxSpeed = 2f;
    public float rotSpeed = 180f;


	// Use this for initialization
	void Start ()
    {
		
	}
	
	// Update is called once per frame
	void Update ()
    {
        //This entire line of coding is for rotating the ship.
        Quaternion rot = transform.rotation;
        float z = rot.eulerAngles.z;
        z -= Input.GetAxis("Horizontal") * rotSpeed * Time.deltaTime;
        rot = Quaternion.Euler(0, 0, z);
        transform.rotation = rot;

        //This entire line of coding is to move the ship.
        Vector3 pos = transform.position;
        Vector3 velocity = new Vector3(0, Input.GetAxis("Vertical") * maxSpeed * Time.deltaTime, 0);
        pos += rot * velocity;
        transform.position = pos;
	}

    //If the player ship collides with the enemy ship, it is destroyed.
    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.tag == "EnemyShip")
        {
            Destroy(gameObject);
        }
    }

    //If the player ship goes out of bounds, it gets destroyed.
    void OnTriggerExit2D(Collider2D other)
    {
        if (other.gameObject.tag == "Board")
        {
            Destroy(gameObject);
        }
    }
}
